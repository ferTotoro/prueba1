package com.example.duoc.fernandaquijada_prueba1.bd;

import com.example.duoc.fernandaquijada_prueba1.clases.Usuario;

import java.util.ArrayList;

/**
 * Created by DUOC on 21-04-2017.
 */

public class BaseDeDatos {
    private static ArrayList<Usuario> values = new ArrayList<>();

    public static void agregarUsuario(Usuario usuario){
        values.add(usuario);
    }

    public static ArrayList<Usuario> obtieneListadoUsuarios(){
        return values;
    }
}
