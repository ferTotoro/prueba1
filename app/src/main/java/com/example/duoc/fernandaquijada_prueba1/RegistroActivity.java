package com.example.duoc.fernandaquijada_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.fernandaquijada_prueba1.bd.BaseDeDatos;
import com.example.duoc.fernandaquijada_prueba1.clases.Usuario;

public class RegistroActivity extends AppCompatActivity {
    private EditText etUsuario, etClave, etClave2;
    private Button btnGuardar, btnVolver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUsuario=(EditText) findViewById(R.id.etUsuarioRegistro);
        etClave=(EditText) findViewById(R.id.etClaveRegistro);
        etClave2=(EditText) findViewById(R.id.etClave2);
        btnGuardar=(Button) findViewById(R.id.btnGuardar);
        btnVolver=(Button) findViewById(R.id.btnVolver);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }

        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUsuario.getText().toString().equals("")||etClave2.getText().toString().equals("")||etClave.getText().toString().equals("")){
                    Toast.makeText(RegistroActivity.this, "Ingrese campos", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(etClave.getText().toString().equals(etClave2.getText().toString())){
                        guardarUsuario();
                        limpiar();
                    }
                    else {
                        Toast.makeText(RegistroActivity.this, "Contraseña no coinciden", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });


    }

    private void limpiar() {
        etClave.setText("");
        etClave2.setText("");
        etUsuario.setText("");
    }

    private void guardarUsuario() {
        Usuario u = new Usuario();
        u.setNombre(etClave2.getText().toString());
        u.setClave(etClave.getText().toString());

        BaseDeDatos.agregarUsuario(u);
        Toast.makeText(this, "Usuario ingresado correctamente", Toast.LENGTH_SHORT).show();
    }
}
