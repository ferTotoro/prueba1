package com.example.duoc.fernandaquijada_prueba1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.duoc.fernandaquijada_prueba1.bd.BaseDeDatos;
import com.example.duoc.fernandaquijada_prueba1.clases.Adaptador;
import com.example.duoc.fernandaquijada_prueba1.clases.Usuario;

import java.util.ArrayList;

public class ListadoUsuariosActivity extends AppCompatActivity {
    private ListView lvUsuario;
    private ArrayList<Usuario> dataSource;
    private Button btnSalir;
    private static ArrayList<Usuario> u= BaseDeDatos.obtieneListadoUsuarios();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);

        lvUsuario = (ListView)findViewById(R.id.lvUsuarios);
        Adaptador adaptador = new Adaptador(this, getDataSource());
        lvUsuario.setAdapter(adaptador);

        lvUsuario.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListadoUsuariosActivity.this,
                        getDataSource().get(position).getNombre(), Toast.LENGTH_LONG).show();
            }
        });


    }

    private ArrayList<Usuario> getDataSource() {
        if(dataSource == null){
            dataSource = new ArrayList<>();

            for(int x = 0; x < u.size(); x++){
                Usuario usu = new Usuario(u.get(x).getNombre(), u.get(x).getClave());
                dataSource.add(usu);
            }
        }

        return dataSource;
    }
}
