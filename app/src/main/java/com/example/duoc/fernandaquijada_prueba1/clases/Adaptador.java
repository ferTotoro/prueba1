package com.example.duoc.fernandaquijada_prueba1.clases;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.duoc.fernandaquijada_prueba1.R;

import java.util.ArrayList;

/**
 * Created by DUOC on 21-04-2017.
 */

public class Adaptador extends ArrayAdapter<Usuario>{
    private ArrayList<Usuario> dataSource;

    public Adaptador(Context context, ArrayList<Usuario> dataSource){
        super(context, R.layout.activity_listado_usuarios, dataSource);
        this.dataSource = dataSource;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View item = inflater.inflate(R.layout.activity_listado_usuarios, null);

        /*TextView tvUsuario = (TextView)item.findViewById(R.id.);
        tvUsuario.setText(dataSource.get(position).getNombre());*/
        return(item);
    }


}
