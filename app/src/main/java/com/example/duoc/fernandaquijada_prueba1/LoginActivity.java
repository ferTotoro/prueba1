package com.example.duoc.fernandaquijada_prueba1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.duoc.fernandaquijada_prueba1.bd.BaseDeDatos;
import com.example.duoc.fernandaquijada_prueba1.clases.Usuario;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private EditText etUsuario, etContrasena;
    private Button btnEntrar, btnRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario=(EditText) findViewById(R.id.etUsuario);
        etContrasena=(EditText) findViewById(R.id.etContrasena);
        btnRegistrarse=(Button) findViewById(R.id.btnRegistrarse);
        btnEntrar=(Button) findViewById(R.id.btnEntrar);

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<Usuario> lista= BaseDeDatos.obtieneListadoUsuarios();

                for (Usuario u:lista){

                    if(etUsuario.getText().toString().equals(u.getNombre().toString()) && etContrasena.getText().toString().equals(u.getClave().toString()))
                    {
                        Intent i=new Intent(LoginActivity.this,ListadoUsuariosActivity.class);
                        startActivity(i);
                    }
                    else{
                        Toast.makeText(LoginActivity.this, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }


}
